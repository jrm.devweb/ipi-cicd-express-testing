FROM node:lts-alpine

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile --production

COPY dist dist

CMD npm start
