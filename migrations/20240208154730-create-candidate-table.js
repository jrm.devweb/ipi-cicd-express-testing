'use strict';

let dbm;
let type;
let seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  db.createTable(
    'candidate',
    {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: 'text',
      email: 'text',
      skills: 'text',
      createdAt: 'text',
    },
    callback,
  );
};

exports.down = function (db, callback) {
  db.dropTable('candidate', callback);
};

exports._meta = {
  version: 1,
};
