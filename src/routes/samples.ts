import express from 'express';
import Sample from '../models/sample';

const router = express.Router();

router.post('/', async (req, res) => {
  await Sample.create(req.body).then(data => res.send(data));
});
router.get('/:id', async (req, res) => {
  await Sample.getOneById(parseInt(req.params.id)).then(data => {
    if (data == null) res.sendStatus(404);
    res.send(data);
  });
});
router.put('/:id', async (req, res) => {
  await Sample.updateOne(parseInt(req.params.id), req.body).then(data => res.send(data));
});
router.delete('/:id', async (req, res) => {
  await Sample.deleteOne(parseInt(req.params.id)).then(data => res.sendStatus(204));
});
router.get('/', async (req, res) => {
  await Sample.getAll().then(data => res.send(data));
});

export default router;
