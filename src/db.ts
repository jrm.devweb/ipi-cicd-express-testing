import sqlite3m from 'sqlite3';

// On aurait pu complexifier davantage, mais avoir juste:
// - une DB de test
// - et une autre de développement
const nodeEnv = process.env.NODE_ENV ?? 'development';
const dbFile = `app-${nodeEnv}.db`;

const sqlite3 = sqlite3m.verbose();

console.log(`Running server with database "${dbFile}"`);
const db = new sqlite3.Database(dbFile);

type Scalar = number | string | boolean;

export async function queryAsync<T = void> (
  sql: string,
  ...args: Scalar[]
): Promise<T[]> {
  return await new Promise((resolve, reject) => {
    const stmt = db.prepare(sql);
    stmt.all(...args, (err: Error | null | undefined, rows: T[]) => {
      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

export async function getLastFromTable<T> (
  table: string,
): Promise<T> {
  const [latest] = await queryAsync(
    `SELECT * FROM ${table} ORDER BY id DESC LIMIT 1`,
  );
  return latest as T;
}

export async function insertIntoTable<T> (
  table: string,
  fields: Partial<T>,
): Promise<T> {
  const keys = Object.keys(fields);
  const keysStr = keys.join(', ');
  const placeholders = new Array(keys.length).fill('?').join(', ');
  const values = Object.values(fields) as Scalar[];
  const sql = `INSERT INTO ${table} (${keysStr}) VALUES (${placeholders})`;
  await queryAsync(sql, ...values);
  return (await getLastFromTable<T>(table)) as T;
}

export async function getOneFromTableById<T> (table: string, id: number): Promise<T | undefined> {
  const [record] = await queryAsync<T>(
    `SELECT * FROM ${table} WHERE id = ?`,
    id,
  );
  return record;
}

export async function updateInTableById<T> (
  table: string,
  id: number,
  updatedFields: Partial<T>,
): Promise<T | undefined> {
  const updates = Object.keys(updatedFields)
    .map((k) => `${k} = ?`)
    .join(', ');
  const sql = `UPDATE ${table} SET ${updates} WHERE id = ?`;
  const args = [...Object.values(updatedFields), id] as Scalar[];
  await queryAsync<T>(sql, ...args);
  return await getOneFromTableById<T>(table, id);
}
export async function deleteInTableById (table: string, id: number): Promise<any | undefined> {
  return await queryAsync(`DELETE FROM ${table} WHERE id = ?`, id);
}
