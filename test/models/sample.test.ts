import { queryAsync } from '../../src/db';
import sampleModel from '../../src/models/sample';
import request from 'supertest';
import app from '../../src/app';

const res = request(app);

async function resetSampleTable () {
  await queryAsync('DELETE FROM sample');
  await queryAsync("UPDATE sqlite_sequence SET seq = 0 WHERE name = 'sample'");
}

describe('Sample tests', () => {
  // Réinitialise la table sample avant chaque test
  beforeEach(async () => {
    await resetSampleTable();
  });

  it('Create sample', async () => {
    const sampleRecord = await sampleModel.create({
      name: `Sample sample ${Date.now()}`,
    });
    expect(sampleRecord).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        name: expect.stringMatching(/^Sample sample \d+$/),
      }),
    );
  });

  it('Gets samples', async () => {
    await Promise.all([
      sampleModel.create({ name: 'sample1' }),
      sampleModel.create({ name: 'sample2' }),
    ]);

    const records = await sampleModel.getAll();
    expect(records).toHaveLength(2);
    expect(records).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          name: 'sample1',
        }),
        expect.objectContaining({
          id: expect.any(Number),
          name: 'sample2',
        }),
      ]),
    );
  });

  describe('getOneById', () => {
    it('Gets an existing sample', async () => {
      const sample = await sampleModel.create({ name: 'sample' });
      const record = await sampleModel.getOneById(sample.id);
      expect(record).toEqual(sample);
    });

    it('Gets a non-existent sample', async () => {
      const record = await sampleModel.getOneById(1111111);
      expect(record).toBeUndefined();
    });
  });

  describe('updateOne', () => {
    it('Updates a sample', async () => {
      const sampleToUpdate = await sampleModel.create({ name: 'Initial' });

      await sampleModel.updateOne(sampleToUpdate.id, { name: 'Updated' });
      const updatedRecord = await sampleModel.getOneById(sampleToUpdate.id);
      expect(updatedRecord).toHaveProperty('id', sampleToUpdate.id);
      expect(updatedRecord).toHaveProperty('name', 'Updated');
    });

    it('Updates nothing if wrong id', async () => {
      const actual = await sampleModel.updateOne(123456, { name: 'Updated' });
      expect(actual).toBeUndefined();
    });
  });

  describe('Crud with router', () => {
    it('post a sample', async () => {
      await res.post('/api/samples').send({ name: 'toto' }).expect({ id: 1, name: 'toto' });
    });
    it('get a sample', async () => {
      await sampleModel.create({ name: 'toto' });
      await res.get('/api/samples/1').expect({ id: 1, name: 'toto' });
    });
    it('get all samples', async () => {
      await sampleModel.create({ name: 'toto' });
      await sampleModel.create({ name: 'tata' });
      await res.get('/api/samples/').expect([{ id: 1, name: 'toto' }, { id: 2, name: 'tata' }]);
    });
    it('update a sample', async () => {
      await sampleModel.create({ name: 'toto' });
      await res.put('/api/samples/1').send({ name: 'tata' }).expect({ id: 1, name: 'tata' });
    });
  });
  it('delete a sample', async () => {
    await sampleModel.create({ name: 'toto' });
    await res.del('/api/samples/1').expect(204);
  });
  it('get with no entry', async () => {
    await res.get('/api/samples/1').expect(404);
  });
  it('getall with no entries', async () => {
    await res.get('/api/samples').expect([]);
  });
});
