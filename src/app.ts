import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import apiRouter from './routes/api';

// Load environment variables from .env file
dotenv.config();

const app = express();

// Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routeur
app.use('/api', apiRouter);

// Autres routes (à supprimer éventuellement)
app.get('/', (req, res) => {
  res.send({ message: 'Hello, World!' });
});
app.get('/:name', (req, res) => {
  console.log(req.params.name);
  res.send({ message: `Hello, ${req.params.name}` });
});

export default app;
