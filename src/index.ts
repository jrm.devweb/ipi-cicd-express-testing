import { port } from './settings';
import app from './app';

// Start the server

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
