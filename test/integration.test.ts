import request from 'supertest';
import app from '../src/app';

describe('Home page integration', () => {
  it('should send "Hello, World!" if no param is given', async () => {
    // Act
    const res = await request(app)
      .get('/')
      // permet de faire une assertion sur le code HTTP de la réponse
      .expect(200);

    // Assert
    expect(res.body).toHaveProperty('message', 'Hello, World!');
  });
  it('should send "Hello, :name!" if param is given', async () => {
    // Act
    const name = 'toto';
    const res = await request(app)
      .get(`/${name}`)
      // permet de faire une assertion sur le code HTTP de la réponse
      .expect(200);

    // Assert
    expect(res.body).toHaveProperty('message', `Hello, ${name}`);
  });
});
